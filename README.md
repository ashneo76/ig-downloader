# ig-downloader

A instagram downloader for images and videos.

## Usage

1. Navigate to the profile page.
2. Click on an image/video.
3. Tap on the page once it loads.
4. Click on the download button.

## Downloads

* Firefox: [Mozilla Addons](https://addons.mozilla.org/en-US/firefox/addon/ig-downloader/)
